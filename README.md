# Session module for NestJS

Nejst module for storing a session. It will setup an express middleware for a session. The session data are stored in a postgres database.

## Usage (server)

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install the package

> Requires [`@uiii-lib/nestjs-config`](https://gitlab.com/uiii-lib/npm/nestjs-config) as peer dependency, so install it too.

```
npm install --save @uiii-lib/nestjs-session
```

Add session module to `imports` of the top level (app) module configure it.

```ts
import { SessionModule } from '@uiii-lib/nestjs-session';
import { PgStoreSessionOptions } from "@uiii-lib/nestjs-session/store/pg";

@Module({
	imports: [
		SessionModule.forRoot<PgStoreSessionOptions>({
			secret: "secret", // session secret, see https://github.com/expressjs/session#secret
			secureCookie: true, // wheter to use secure session cookie, see https://github.com/expressjs/session#cookiesecure
			storeType: 'pg' // session store, see below
			storeOptions: { // session store options
				...
			}
		})
	],
	...
})
export class AppModule {}
```

> *For development it is possible to omit `storeType` option to use the default in-memory store (**not suitable for production!**)*

Access the session on request object

```ts
@Controller()
class MyController {
	@Get()
	findAll(@Req() request: Request) {
  		request.session.visits = request.session.visits ? request.session.visits + 1 : 1;
	}
}
```

## Store

Session module currently supports only PostgreSQL store. It is set by the session module's `storeType` option.

### PostgreSQL

```ts
storeType: 'pg'
```

> Requires [`@uiii-lib/nestjs-database-postgres`](https://gitlab.com/uiii-lib/npm/nestjs-database-postgres) as peer dependency, so install it.

#### Configuration

```ts
storeOptions: {
	initDb: true, // whether to create DB schema automatically
	schemaName: "schema", // name of the DB schema where the session table is
	tableName: "schema", // name of the DB table to store the sessions data
}
```

If `initDb` is test to `true`, the module will automatically initialize the database table for storing sessions. Otherwise you have to create the table manually, see `src/store/pg.store.ts`.

### Redis

```ts
storeType: 'redis'
```

> Requires [`ioredis`](https://gitlab.com/uiii-lib/npm/nestjs-database-postgres) as peer dependency, so install it.

#### Configuration

```ts
storeOptions: {
	client: new Redis({ // Redis client connected to the DB where to store session
		host: "redis",
		port: 6397,
	})
	prefix: "auth:" // prefix keys of session records in Redis (default: "sess:")
}
```
