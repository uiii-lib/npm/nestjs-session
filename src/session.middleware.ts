import session from "express-session";

import {
	Inject,
	Injectable,
	NestMiddleware,
	OnModuleInit,
	Optional,
} from "@nestjs/common";

import {
	MODULE_OPTIONS_TOKEN,
	SessionModuleOptions,
} from "./session.module-definition.js";

@Injectable()
export class SessionMiddleware implements NestMiddleware, OnModuleInit {
	protected session: any;

	constructor(
		@Inject(MODULE_OPTIONS_TOKEN) private options: SessionModuleOptions,
		@Optional() private store?: session.Store,
	) {}

	// init session later after the database migrations are applied
	onModuleInit() {
		this.session = session({
			secret: this.options.secret,
			resave: false,
			rolling: true,
			saveUninitialized: true,
			proxy: true,
			cookie: {
				secure: this.options.secureCookie,
				maxAge: 365 * 24 * 60 * 60 * 1000, // one year
				sameSite: "lax",
			},
			store: this.store,
		});
	}

	use(req: any, res: any, next: () => void) {
		if (!this.session) {
			next();
		}

		this.session(req, res, next);
	}
}
