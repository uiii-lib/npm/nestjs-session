import { ConfigurableModuleBuilder } from "@nestjs/common";

export interface SessionModuleNonAsyncableOptions {
	storeType?: "mongo" | "pg" | "redis";
}

export interface SessionModuleOptions {
	secret: string;
	secureCookie?: boolean;
	storeOptions?: any;
}

export const {
	ConfigurableModuleClass,
	MODULE_OPTIONS_TOKEN,
	OPTIONS_TYPE,
	ASYNC_OPTIONS_TYPE,
} = new ConfigurableModuleBuilder<SessionModuleOptions>()
	.setClassMethodName("forRootInternal")
	.setExtras<SessionModuleNonAsyncableOptions>({ storeType: undefined })
	.build();
