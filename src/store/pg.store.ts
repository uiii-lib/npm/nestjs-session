import ConnectPgStore from "connect-pg-simple";
import session from "express-session";

import {
	Inject,
	Injectable,
	ModuleMetadata,
	OnModuleInit,
} from "@nestjs/common";
import {
	DatabaseModule,
	DatabaseService,
} from "@uiii-lib/nestjs-database-postgres";

import {
	MODULE_OPTIONS_TOKEN,
	SessionModuleOptions,
} from "../session.module-definition.js";

export interface PgStoreSessionOptions extends SessionModuleOptions {
	storeOptions: {
		initDb: boolean;
		schemaName: string;
		tableName: string;
	};
}

@Injectable()
export class PgStore extends ConnectPgStore(session) implements OnModuleInit {
	constructor(
		protected databaseService: DatabaseService,
		@Inject(MODULE_OPTIONS_TOKEN)
		protected options: PgStoreSessionOptions,
	) {
		super({
			schemaName: options.storeOptions.schemaName,
			tableName: options.storeOptions.tableName,
			pool: databaseService.client(),
		});
	}

	async onModuleInit() {
		const { initDb, schemaName, tableName } = this.options.storeOptions;

		if (initDb) {
			const queryBuilder = this.databaseService.build();

			const schemaNameRef = queryBuilder.ref(schemaName);
			const tableNameRef = queryBuilder.ref(tableName).withSchema(schemaName);

			await this.databaseService.client().query(`
				CREATE SCHEMA IF NOT EXISTS ${schemaNameRef};

				CREATE TABLE IF NOT EXISTS ${tableNameRef} (
					"sid" varchar NOT NULL COLLATE "default",
					"sess" json NOT NULL,
					"expire" timestamp(6) NOT NULL,
					CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE
				)
				WITH (OIDS=FALSE);

				CREATE INDEX IF NOT EXISTS "IDX_session_expire" ON ${tableNameRef} ("expire");
			`);
		}
	}
}

export const moduleMetadata: ModuleMetadata = {
	imports: [DatabaseModule],
	providers: [
		PgStore,
		{
			provide: session.Store,
			useExisting: PgStore,
		},
	],
	exports: [PgStore, session.Store],
};

export default moduleMetadata;
