import ConnectMongoStore from "connect-mongo";
import session from "express-session";
import { MongoClient } from "mongodb";

import { ModuleMetadata } from "@nestjs/common";

import {
	MODULE_OPTIONS_TOKEN,
	SessionModuleOptions,
} from "../session.module-definition.js";

export interface MongoStoreSessionOptions extends SessionModuleOptions {
	storeOptions: {
		client: MongoClient;
		database?: string;
	};
}

export const moduleMetadata: ModuleMetadata = {
	providers: [
		{
			provide: session.Store,
			useFactory: (options: MongoStoreSessionOptions) => {
				return ConnectMongoStore.create({
					client: options.storeOptions.client,
					dbName: options.storeOptions.database,
				});
			},
			inject: [MODULE_OPTIONS_TOKEN],
		},
	],
	exports: [session.Store],
};

export default moduleMetadata;
