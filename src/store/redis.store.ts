import ConnectRedisStore from "connect-redis";
import session from "express-session";
import { Redis } from "ioredis";

import { Inject, Injectable, ModuleMetadata } from "@nestjs/common";

import {
	MODULE_OPTIONS_TOKEN,
	SessionModuleOptions,
} from "../session.module-definition.js";

export interface RedisStoreSessionOptions extends SessionModuleOptions {
	storeOptions: {
		client?: Redis;
		prefix?: string;
	};
}

@Injectable()
export class RedisStore extends ConnectRedisStore {
	constructor(
		@Inject(MODULE_OPTIONS_TOKEN)
		protected options: RedisStoreSessionOptions,
	) {
		super({
			client: options.storeOptions.client,
			prefix: options.storeOptions.prefix || "sess:",
		});
	}
}

export const moduleMetadata: ModuleMetadata = {
	providers: [
		RedisStore,
		{
			provide: session.Store,
			useExisting: RedisStore,
		},
	],
	exports: [RedisStore, session.Store],
};

export default moduleMetadata;
