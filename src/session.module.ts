import {
	ConfigurableModuleAsyncOptions,
	DynamicModule,
	MiddlewareConsumer,
	Module,
	ModuleMetadata,
} from "@nestjs/common";
import { ConfigModule } from "@uiii-lib/nestjs-config";

import { SessionMiddleware } from "./session.middleware.js";
import {
	ASYNC_OPTIONS_TYPE,
	ConfigurableModuleClass,
	OPTIONS_TYPE,
	SessionModuleOptions,
} from "./session.module-definition.js";

@Module({
	imports: [ConfigModule],
})
export class SessionModule extends ConfigurableModuleClass {
	static async forRoot<T extends SessionModuleOptions>(
		options: typeof OPTIONS_TYPE & T,
	): Promise<DynamicModule> {
		return mergeModuleMetadata(
			this.forRootInternal(options),
			await SessionModule.importStoreModule(options.storeType),
		);
	}

	static async forRootAsync<T extends SessionModuleOptions>(
		options: typeof ASYNC_OPTIONS_TYPE &
			ConfigurableModuleAsyncOptions<T, "create">,
	): Promise<DynamicModule> {
		return mergeModuleMetadata(
			this.forRootInternalAsync(options),
			await SessionModule.importStoreModule(options.storeType),
		);
	}

	async configure(consumer: MiddlewareConsumer) {
		consumer.apply(SessionMiddleware).forRoutes("*");
	}

	protected static async importStoreModule(
		storeType?: string,
	): Promise<ModuleMetadata> {
		if (!storeType) {
			console.warn(
				"WARNING: No session store is set which default to memory. Not suitable for production!!",
			);

			return {};
		}

		const store = await import(`./store/${storeType}.store.js`);

		return store.moduleMetadata;
	}
}

function mergeModuleMetadata(
	first: DynamicModule,
	...other: ModuleMetadata[]
): DynamicModule {
	return other.reduce<DynamicModule>((result, it) => {
		for (const key of Object.keys(it) as (keyof ModuleMetadata)[]) {
			result = {
				...result,
				[key]: [...(result[key] || ([] as any)), ...(it[key] || ([] as any))],
			};
		}

		return result;
	}, first);
}
